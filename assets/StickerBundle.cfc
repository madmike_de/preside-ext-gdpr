component {

	public void function configure( bundle ) {
		bundle.addAsset( id="gdpr-modal-css"   , path="/css/specific/gdpr/gdpr-modal.css"    );
		bundle.addAsset( id="gdrp-toggle-css"  , path="/css/specific/gdpr/bootstrap-toggle.min.css"    );
		bundle.addAsset( id="gdpr-js"          , path="/js/specific/gdpr/gdpr.js"       );
		bundle.addAsset( id="gdrp-toggle-js"   , path="/js/specific/gdpr/bootstrap-toggle.min.js"       );

		bundle.asset( "gdpr-js" ).after( "*" );
		bundle.asset( "gdrp-toggle-js" ).after( "*" );
		bundle.asset( "gdrp-toggle-css" ).after("*");
		bundle.asset( "gdpr-modal-css" ).dependsOn("gdrp-toggle-css");
	}

}