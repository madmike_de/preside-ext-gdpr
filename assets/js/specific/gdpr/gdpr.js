$(function() {
  if(cfrequest.showCookieConsentModal == true) {
    setTimeout(function() {
      $('#lab-slide-bottom-popup').modal('show');
    }, 1000); // optional - automatically opens in xxxx milliseconds
  
  }
  
  $('form.gdprSettings, .gdprSettingsFormContainer').on("change", ".gdprGroup", function(e) {
    var group = $(this).data('group');
    var selValue = $(this).prop('checked');
    
    $("input.gdprItem[data-group*='" + group + "']").prop('checked', selValue).change();
    
  })
  
  $('#gdprAcceptAll').on("click", function () {
    $.get( "/gdpr/AcceptAllPermissions/" );
  });
  
  $('#gdprChangeSettings').on("click", function () {
    $.get( "/gdpr/getGdprForm/", function( data ) {
      $( ".gdprSettingsFormContainer" ).html( data ).show();
      $('.gdprSettings').find('[data-toggle="toggle"]').bootstrapToggle();
    } );
  });  
  
  $('.gdprSettingsFormContainer').on("submit", "form.gdprSettings", function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');

    $.ajax({
      type: "POST",
      url: url + '?NoNextEvent=true',
      data: form.serialize(), // serializes the form's elements.
      success: function(data)
      {
        $('#lab-slide-bottom-popup').modal('hide');
      }
    });
  });
    
});