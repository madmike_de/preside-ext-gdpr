<cfoutput>
	<cfif args.showCookieConsentModal>
		<div class="modal fade" id="lab-slide-bottom-popup" data-keyboard="false" data-backdrop="false">
			<div class="lab-modal-body">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				
				#renderContent( "richeditor", prc.gdprSettings.disclaimer )#
				
				<div class="text-center">
					<button id="gdprChangeSettings" type="button" class="btn-link btn btn-lg">#prc.gdprSettings.configBtnTxt#</button>
					<button id="gdprAcceptAll" type="button" class="btn-primary btn-plain btn btn-lg" data-dismiss="modal">#prc.gdprSettings.acceptAllBtnTxt#</button>
				</div>

				<div class="gdprSettingsFormContainer"></div>
			</div>
			<!-- /.modal-body -->
		</div>
		
	</cfif>
</cfoutput>