
<cfoutput>
	<form name="gdprSettings" class="form form-horizontal gdprSettings" action="#event.buildLink(linkTo='gdpr.saveItemPermissions')#" method="post">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

			#renderForm(
					formName         = args.gdprFormName
					, fieldLayout    = "formcontrols.layouts.gdprfield"
					, context        = "website"
				)#

		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<br><button class="btn btn-primary" type="submit">Einstellungen speichern</button>
			</div>
		</div>
	</form>
</cfoutput>