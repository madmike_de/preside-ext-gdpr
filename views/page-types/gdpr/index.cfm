<cfparam name="args.title"        		type="string"	field="page.title"        		editable="false" />
<cfparam name="args.main_content"			type="string"	field="page.main_content" 		editable="false" />
<cfscript>
	saveSettingsBtnTxt = getSystemSetting( category="gdprsettings", setting="saveSettingsBtnTxt", default=translateResource( uri="system-config.gdprsettings:saveSettingsBtnTxt.default" ) );
</cfscript>
<cfoutput>
	
	#renderContent( "richeditor", args.main_content )#
	
	<cfif isDefined("rc.ErrCode")>
		<cfswitch expression="#rc.ErrCode#">
			<cfcase value="0">
				<div class="alert alert-success" role="alert">Ihre Änderungen wurden gespeichert.</div>
			</cfcase>
			<cfcase value="1">
				<div class="alert alert-danger" role="alert">Beim speichern trat ein Problem auf.</div>
			</cfcase>
			<cfdefaultcase>
				<div class="alert alert-warn" role="alert">Unbekannter Fehler beim speichern</div>
			</cfdefaultcase>
		</cfswitch>
		
	</cfif>

		<form name="gdprSettings" class="form form-horizontal gdprSettings" action="#event.buildLink(linkTo='gdpr.saveItemPermissions')#" method="post">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					
			#renderForm(
					formName         = args.gdprFormName
					, fieldLayout    = "formcontrols.layouts.gdprfield"
					, context        = "website"
				)#

			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<br><button class="btn btn-primary" type="submit">#saveSettingsBtnTxt#</button>
				</div>
			</div>
		</form>
</cfoutput>