<cfscript>
	inputName    = args.name              ?: "";
	inputId      = args.id                ?: "";
	inputClass   = args.class             ?: "";
	defaultValue = args.defaultValue      ?: "";
	group        = args.group             ?: "";
	mandatory    = val(args.mandatory)    ?: false;

	value  = event.getValue( name=inputName, defaultValue=defaultValue );
	if ( not IsSimpleValue( value ) ) {
		value = "";
	}

	checked = IsBoolean( value ) and value;
	
	if(mandatory) checked = true;
</cfscript>

<cfoutput>
	<input class="#inputClass#" data-toggle="toggle" data-group="#group#" data-size="small" data-class="fast" data-onstyle="success" data-offstyle="danger" type="checkbox"<cfif mandatory> disabled</cfif> name="#inputName#"<cfif checked> checked="checked"</cfif> value="1" tabindex="#getNextTabIndex()#">
	<cfif mandatory><input name="#inputName#" type="hidden" value="1"/></cfif>
	<span class="lbl"></span>
</cfoutput>