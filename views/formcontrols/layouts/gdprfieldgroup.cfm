<cfscript>
	param name="args.control"     type="string";
	param name="args.label"       type="string";
	param name="args.description" type="string";
	param name="args.help"        type="string";
	param name="args.for"         type="string";
	param name="args.error"       type="string";
	param name="args.count"       type="numeric";
</cfscript>

<cfoutput>

	<div class="panel panel-default">
    <div class="panel-heading" role="tab">
      <div class="panel-title">
        
					<div class="row">
							<div class="col-sm-10" for="#args.for#">
								<b class="gdprGroupLabel" data-toggle="collapse" data-parent="##accordion" href="##collapse#args.count#" aria-expanded="false" aria-controls="collapse#args.count#">#args.label#</b>
							</div>
							<div class="col-sm-2 text-right">
								#args.control#
							</div>
					</div>
	       </a>
				</div>
    </div>
    <div id="collapse#args.count#" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading#args.count#">
      <div class="panel-body">
			
				#args.description#
</cfoutput>
