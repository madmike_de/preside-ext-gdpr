<cfscript>
	param name="args.control"  type="string";
	param name="args.label"    type="string";
	param name="args.description" type="string";
	param name="args.help"     type="string";
	param name="args.for"      type="string";
	param name="args.error"    type="string";
	param name="args.required" type="boolean";
	param name="args.provider" type="string";
	param name="args.duration" type="string";

	hasError = Len( Trim( args.error ) );
</cfscript>

<cfoutput>
	<div class="gdprItem">
		<div class="form-group<cfif hasError> has-error</cfif>">
			<div class="col-xs-10" for="#args.for#">
				<div class="gdprLabel">#args.label#</div>
				<div class="gdprDescription">#renderContent( "richeditor", args.description )#</div>
				<cfif !isEmpty(args.provider)><div class="gdprProvider"><span>Anbieter:</span> #args.provider#</div></cfif>
				<cfif !isEmpty(args.duration)><div class="gdprDuration"><span>Laufzeit:</span> #args.duration#</div></cfif>
			</div>

			<div class="col-xs-2 text-right">
					#args.control#
			</div>
		</div>
	</div>
</cfoutput>
