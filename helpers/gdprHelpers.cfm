	<cfscript>
		public boolean function gdprIsAllowed(shortcode) {
			var gdprService       = getSingleton( "gdprService" );

			if(gdprService.isBot()) return false;
			
			var visitorService    = getSingleton( "WebsiteVisitorService" );
			var poService         = getSingleton( "presideObjectService" );
			
			return gdprService.isAllowed( arguments.shortcode?:"" );
		}
	</cfscript>