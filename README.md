# Preside GDPR Cookie Extension

The new GDPR rules require to manage cookie more specific. Meanwhile it's not enough to say "Yeah Cookies, go ahead", you need to make them available for separate sections and allow the user to allow/deny every single cookie. 

This tiny extension allows you to define all cookies and trackers you use on your website. A configuration page allows the (anonymize) visitor to allow/deny each single setting.

In your code you can check every setting for a user and react accordingly to show maybe some information or just leave out the code which tracks your visitor.


Attention: This module requires Bootstrap and jQuery to work.

## Installation
Install the extension to your application via either of the methods detailed below (Git submodule / CommandBox + ForgeBox)

### Git Submodule method
From the root of your application, type the following command:

	git submodule add https://gitlab.com/madmike_de/preside-ext-gdpr.git application/extensions/preside-ext-gdpr

### CommandBox (box.json) method
From the root of your application type the following command:

	box install preside-ext-gdpr

From the Preside CMS developer console reload the application:

	reload all


## Usage
### The visitor

The most funny thing about the GDPR cookie stuff is that you need to identify the individual user. This is happening with a - right: Cookie.
The visitor ist completely anonymous and is automatically generated as a UUID using the Preside [visitorService.getVisitorId()](https://docs.preside.org/reference/api/websitevisitorservice/getvisitorid.html).

As long as the VisitogID exists, the saved settings are checked and validated. When the VisitorID changes or the settings are deleted for this VisitorID the GDPR Dialog is shown again and all(!!) cookies are denied until the user allows them again.


### Available itemShortCodes
The item short codes are predefined as an enum in the *config/config.cfc* file:

	settings.enum.gdprItemShortcode  = [ "ga","matomo","gads","yt","vimeo","vid","cfid","cftoken","jsession" ];

This is done to avoid user errors and to allow only a selection of predefined values.
If you need more values you can simply append them to the array in your own *config.cfc* with *arrayAppend*.

### Objects

Three datamanager objects are definied by the extension:

* **gdprgroup**: Define groups for organizing your cookies, like *Statistics*, *tracking*, etc.
* **gdpritem**: Define the individual GDPR items you allow/deny for the visitor.
* **gdprpermission**: Here are the saved permissions per item and visitor. In your code you can check if the visitor allowed/denied the item.

### Pagetype

A  pagetype **gdpr** is defined as a systempage. This page is showing a form with all **grpritem**s, grouped by **grprgroup** and allows the user to allow/deny each single setting. The permissions are saved in the object **gdprpermission** per individual user.


### System settings

There are several system setting for button labels, etc.


## Show cookie consent dialog

To show the annoying dialog to a visitor, asking for permission, do the following:

put the viewlet code at the start of your layout (must be before the first `renderIncludes()`):

	gdprDialog = renderViewlet( event="gdpr._gdprDialog" );

at the end of your layout output the viewlet (must be before `renderIncludes("js")`):

	#gdprDialog#

If the automatic generated  [visitorID](https://docs.preside.org/reference/api/websitevisitorservice/getvisitorid.html) can't be found in the **gdprpermission** object the dialog will be shown. Otherwise the visitor already did some settings and the dialog will not be shown.


## Check permissions

Each gdpr item has a shortcode. This shortcode can be used as a simple identifier of the item. The shortcode can be used in the validation function. To check if a user allowed or denied the access of a permission use the following:

	gdprIsAllowed('shortcode')

The functions return true/false when the permission for this item is allowed/denied.

### Todos

- Systemsetting for how long the cookie settings are stored.
- Automatic task deleting settings older than specified.



### License
Code released under the [MIT licence](http://opensource.org/licenses/MIT).

See [SilkTide licence](https://silktide.com/tools/cookie-consent/docs/license/) for details.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
