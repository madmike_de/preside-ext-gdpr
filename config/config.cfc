component {

	public void function configure( required struct config ) {
		var settings = arguments.config.settings?:{};

		settings.enum.gdprItemShortcode  = [ "ga","matomo","gads","yt","vimeo","vid","cfid","cftoken","jsession" ];

	}
}
