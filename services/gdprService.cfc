
component {
	property name="GroupDao"       inject="presidecms:object:gdprgroup";
	property name="ItemDao"        inject="presidecms:object:gdpritem";
	property name="PermissionDao"  inject="presidecms:object:gdprpermission";
	property name="visitorService" inject="WebsiteVisitorService";
	property name="formsService"   inject="formsService";
	property name="sessionStorage" inject="coldbox:plugin:sessionStorage";

	public boolean function isBot() {
		var UserAgent = CGI.http_user_agent;
		
		var arBadBots = [
			"bot",
			"crawler", 
			"spider", 
			"slurp", 
			"mediapartners-google"
		];
		
		for (bot in arBadBots) {
			if(reFindNoCase(bot,UserAgent)) {
				return true;
			}
		}
	
		return false;
	}
	
	public boolean function isAllowed(required string shortCode) {
		
		if(!sessionStorage.exists( "gdpr")) {
			var result = PermissionDao.selectData(
				filter       = { "allowed" = 1, visitorID = visitorService.getVisitorId()}
			, selectFields = [ "item.shortcode"]
			);		

			sessionStorage.setVar( "gdpr", ValueArray( result.shortcode ) ); ;
		}

		var permissions = sessionStorage.getVar( "gdpr");
		
		if(isArray(permissions) && arrayFindNoCase(permissions,arguments.shortcode)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	
	public query function getGroups(any shortcode="" ) {
		var filter = { isActive = true };
		if(!isEmpty(arguments.shortcode)) {
			filter = { shortcode = arguments.shortcode }
		}
		
		var result = GroupDao.selectdata(filter = filter, orderBy = "isMandatory desc, label asc");
		return result;
	}

	public query function getItems(any shortcode="" ) {
		var filter = { isActive = true };
		if(!isEmpty(arguments.shortcode)) {
			filter = { shortcode = arguments.shortcode }
		}

		var result = ItemDao.selectdata(filter = filter, orderBy = "label asc");
		return result;
	}

	public query function getItemsForGroup(required string id="" ) {
		// TODO: The following query needs some performance optimization. Like is not the best choice here.
		var result = ItemDao.selectdata(
				selectFields = ["id","label","shortcode","description","privacyLink","group_concat( distinct groups.id ) as grouplist","isMandatory", "provider", "duration"]
				, groupBy = "id"
				, having       = "grouplist like :groups"
				, filter = { "isActive" = true }
				, filterParams = { "groups" = "%#arguments.id#%" }	
		);
		return result;
	}

	public function getGroupsWithItems(any shortcode="" ) {
		var groups = getGroups(arguments.shortcode);
		var result = [];
		var temp = {};
		
		for(g in groups) {
			temp = g;
			temp.items = getItemsForGroup(g.id);
			arrayAppend(result,temp);
		}
		
		return result;
	}

	public struct function getVisitorSettings() {
		var ret = {};
		
		var result = PermissionDao.selectData(
				 	filter       = { visitorID = visitorService.getVisitorId()}
				, selectFields = [ "item.id", "allowed"]
			);

		// turn result into a struct
		for(var i in result) {
			ret[i.id]=i.allowed;
		}

		return ret;
	}
	
	public function createGdprSettingsForm() {
		var gdprGroups = getGroupsWithItems();
		var visitorSettings = getVisitorSettings();

		var theForm = formsService.createForm( formName="gdprSettingsForm", generator=function( formDefinition ){
			var vCount=1;
						
			for(var group in gdprGroups) {
				
				formDefinition.addField(
					tab           = "default"
					, fieldset    = "default"
					, name        = "group_#group.id#"
					, label       = group.label
					, description = group.description
					, group       = group.id
					, layout      = "formcontrols.layouts.gdprfieldgroup"
					, control     = "gdprYesNoSwitch"
					, class       = "gdprGroup"
					, mandatory   = group.ismandatory
					, count       = vCount
				);
				
				for(var item in group.items) {
				
					formDefinition.addField(
						tab            = "default"
						, fieldset     = "default"
						, name         = "item_#item.id#"
					  , label        = item.label
					  , description  = item.description
					  , group        = item.grouplist
						, control      = "gdprYesNoSwitch"
					  , class        = "gdprItem"
				    , layout       = "formcontrols.layouts.gdprfielditem"
						, mandatory    = val(item.ismandatory)?:group.ismandatory
						, provider     = item.provider
						, defaultvalue = visitorSettings[item.id]?:0
						, duration     = item.duration
					);
				}
				
				formDefinition.addField(
					tab           = "default"
					, fieldset    = "default"
					, name        = "groupend_#group.id#"
					, layout      = "formcontrols.layouts.gdprfieldgroup_end"
				);
				
				vCount++;
			}
		} );
		return theForm;
	}

}