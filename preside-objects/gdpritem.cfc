/**
 * @multilingual true
 * @siteFiltered true
 * @dataManagerGroup cookieconsent
 * @datamanagerGridFields  label,shortcode,datecreated,datemodified
 * @datamanagerAllowDrafts false
 * @versioned false
 */
component {
	property name="label"       type="string"  dbtype="varchar" maxlength=100 required=true  multilingual="true";
	property name="shortcode"   type="string"  dbtype="varchar" required=true  multilingual="false" enum="gdprItemShortcode";
	property name="description" type="string"  dbtype="text"                  required=false multilingual="true";
	property name="privacyLink" control="linkPicker" required="false" multilingual="true";
	property name="isActive"    type="boolean" dbtype="boolean" default=true  required=false;
	property name="groups"      relationship="many-to-many" relatedto="gdprgroup" required=false relatedVia="gdpr_group_item";
	property name="provider"    type="string"  dbtype="varchar" maxlength=100 required=false multilingual="false";
	property name="duration"    type="string"  dbtype="varchar" maxlength=20  required=false multilingual="false";
	property name="isMandatory" type="boolean" dbtype="boolean" default=false required=false;
	property name="isActive"    type="boolean" dbtype="boolean" default=true  required=false;
}