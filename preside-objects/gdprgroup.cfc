/**
 * @multilingual true
 * @siteFiltered true
 * @dataManagerGroup cookieconsent
 * @datamanagerGridFields  shortcode,label,datecreated,datemodified
 * @datamanagerAllowDrafts false
 * @versioned false
 */
component {
	property name="label" type="string" dbtype="varchar" maxlength=100 required=true multilingual="true";
	property name="shortcode" type="string" dbtype="varchar" maxlength=10 required=true multilingual="false";
	property name="description" type="string" dbtype="text" required="false" multilingual="true";
	property name="items" relationship="many-to-many" relatedto="gdpritem" required=false relatedVia="gdpr_group_item";
	property name="isMandatory"   type="boolean" dbtype="boolean" default="false" required=false;
	property name="isActive"   type="boolean" dbtype="boolean" default="true" required=false;
	
}
