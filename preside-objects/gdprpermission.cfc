/**
 * @siteFiltered true
 * @dataManagerGroup cookieconsent
 * @datamanagerGridFields  visitorid,item,allowed,datecreated
 * @noLabel true
 * @datamanagerDisallowedOperations add,edit,clone
 * @datamanagerAllowDrafts false
 * @versioned false
 * @cacheMaxObjects    10000
 * @cacheReapFrequency 5
 * @cacheEvictCount    2000
 */

component {
	property name="item"  					relationship="many-to-one"	relatedTo="gdpritem"	required=true indexes="gdprpermission|1";
	property name="visitorid"	 			type="string" 		dbtype="varchar" maxlength="50" required=true indexes="gdprvisitorid,gdprpermission|2";
	property name="allowed"					type="boolean" dbtype="boolean" default="false";
}