
component {
	property name="gdprService" inject="gdprService";
	property name="visitorService" inject="WebsiteVisitorService";
	property name="PermissionDao" inject="presidecms:object:gdprpermission";
	property name="sessionStorage" inject="coldbox:plugin:sessionStorage";


	public query function getGroups(event, rc, prc, args={} ) {
		return gdprService.getGroups(rc.shortcode?:"");
	}

	public query function getItems(event, rc, prc, args={} ) {
		return gdprService.getItems(rc.shortcode?:"");
	}

	public function getGroupsWithItems(event, rc, prc, args={} ) {
		return gdprService.getGroupsWithItems(rc.shortcode?:"");
	}

	private string function _gdprDialog( event, rc, prc, arg={} ) {
		
		if(gdprService.isBot()) {
			return "";
		}
		
		var args = duplicate( arguments.args );

		if(isDefined('prc.presidepage.page_type') && prc.presidepage.page_type == "gdpr") {
			// Don't show Dialog on settings page
			args.showCookieConsentModal = false;
		} else {
			// Check if visitor has already a record in DB
			var check = PermissionDao.selectData(filter = {visitorid = visitorService.getVisitorId() },recordCountOnly=true);

			args.showCookieConsentModal = val(check)==0?true:false;
		}

		// Get Settings
		prc.gdprSettings = {};
		prc.gdprSettings.configBtnTxt    = getSystemSetting(category = "gdprsettings" , setting  = "configBtnTxt"    , default=translateResource( uri="system-config.gdprsettings:configBtnTxt.default"));
		prc.gdprSettings.acceptAllBtnTxt = getSystemSetting(category = "gdprsettings" , setting  = "acceptAllBtnTxt" , default=translateResource( uri="system-config.gdprsettings:acceptAllBtnTxt.default"));
		prc.gdprSettings.disclaimer      = getSystemSetting(category = "gdprsettings" , setting  = "disclaimer"      , default=translateResource( uri="system-config.gdprsettings:disclaimer.default"));


		event.includeData( data={ showCookieConsentModal = args.showCookieConsentModal } )
		     .include( assetId="gdpr-modal-css" )
				 .include( assetId="gdpr-js" )
		     .include( assetId="gdrp-toggle-css" )
				 .include( assetId="gdrp-toggle-js" )
				 ;

		return renderView( view="/general/_gdprDialog", args=args );
	}

	/*
	public void function test() {
		dump(var = getGroups(), label = "getGroups()");
		dump(var = getItems(), label = "getItems()");
		dump(var = getGroupsWithItems(), label = "getGroupsWithItems()");
		dump(var = sessionStorage.getVar( "gdpr", [] ), label = "SessionStorage:gpdr");
		abort;
	}
	*/
	
	public function AcceptAllPermissions(event, rc, prc, args={} ) {
		var visitorID = visitorService.getVisitorId(); // create VistorID

		var itemList = getItems();

		for(var item in itemList) {
			// Permission exists?
			check = PermissionDao.selectData(filter = {item = item.id,visitorid = visitorId},recordCountOnly=true);
			if(val(check)) {
				result = PermissionDao.updateData(
				data = {allowed = 1},
				filter = {item = item.id,visitorid = visitorId}
				);
			} else {
				result = PermissionDao.insertData(
				data = {
						item = item.id,
						visitorid = visitorId,
						allowed = 1
					}
				);
			}

		}
		sessionStorage.deleteVar( "gdpr" );
		return true;
	}

	public function saveItemPermissions(event, rc, prc, args={} ) {
		var visitorID = visitorService.getVisitorId(); // create VistorID
		var formData  = event.getCollectionForForm();  // get Items from form submission

		var itemList = formData.keyList();
		var result = "";
		var check ="";
		var vErrCode = 0;

		try {
			
			for(var formItem in itemList) {
				if(listfirst(formItem,"_") == "item") { // only save item elements, not Group elements
					
					// Permission exists?
					check = PermissionDao.selectData(filter = {item = listLast(formItem,"_"),visitorid = visitorId},recordCountOnly=true);
					if(val(check)) {
						result = PermissionDao.updateData(
							data = {allowed = val(listfirst(formData[formItem]))},  // Take the first value. Happens, if one cookie is used in more than one group.
							filter = {item = listLast(formItem,"_"),visitorid = visitorId}
							);
					} else {
						result = PermissionDao.insertData(
							data = {
								item = listLast(formItem,"_"),
								visitorid = visitorId,
								allowed = val(formData[formItem])
							}
						);
					}
				}
			}
				
		} catch (any e) {
			vErrCode = 1;
		}

		if(rc.NoNextEvent?:false) {
			return vErrCode;
		}

		sessionStorage.deleteVar( "gdpr" );
		setNextEvent( url = event.buildLink( page="gdpr" ), persistStruct = {errcode:vErrCode} );		
	}

	remote function getGdprForm(event, rc, prc, args={}) {
		event.noLayout();
		args.gdprFormName = gdprService.createGdprSettingsForm();
		
		return renderView(
				view          = "/general/_gdprDialogSettings"
			, args          = args
		);
	}
	
	
}