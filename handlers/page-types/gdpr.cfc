
component {
	property name="gdprService" inject="gdprService";
	
	public function index(event, rc, prc, args={} ) {
		event.includeData( data={ showCookieConsentModal = false } )
					.include( assetId="gdpr-modal-css" )
					.include( assetId="gdpr-js" )
					.include( assetId="gdrp-toggle-css" )
					.include( assetId="gdrp-toggle-js" )
					;

		args.gdprFormName = gdprService.createGdprSettingsForm();
		
		return renderView(
				view          = "/page-types/gdpr/index"
			, presideObject = "page"
			, id            = event.getCurrentPageId()
			, args          = args
		);

	}

}